package moha.mycompany.com.myaccessibilityservice;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.EditText;

public class AccessibilityServ extends AccessibilityService {

	String TAG = "sthh";

	@Override
	public void onAccessibilityEvent(AccessibilityEvent event) {


//			if (event.getPackageName().toString().equals("com.android.chrome")) {
//				StringBuilder message = new StringBuilder();
//				if (!event.getText().isEmpty()) {
//					for (CharSequence subText : event.getText()) {
//						message.append(subText);
//					}
//					if (message.toString().contains("Message from: ")) {
//						name = message.toString().substring(13);
//					}
//				}
//			}

		AccessibilityNodeInfo currentNode = getRootInActiveWindow();

		Log.e(TAG, "******************************");

//		Log.e(TAG, "toString     : " + currentNode.toString());
		Log.e(TAG, "getMainChildCount: " + currentNode.getChildCount());
		for (int i = 0; i < currentNode.getChildCount(); i++) {
			Log.e(TAG, "PreChild1 " + i + "  : " + currentNode.getChild(i).getClassName());
			Log.e(TAG, "Childrens    : " + currentNode.getChild(i).getChildCount());
			for (int j = 0; j < currentNode.getChild(i).getChildCount(); j++) {
				Log.e(TAG, "PreChild2 " + j + "  : " + currentNode.getChild(i).getChild(j).getClassName());
//				for (int k = 0; k < currentNode.getChild(i).getChild(j).getChildCount(); k++) {
//					Log.e(TAG, "naveh     " + k + "  : " + currentNode.getChild(i).getChild(j).getChild(k).getClassName());
//				}

//				Log.e(TAG, currentNode.getChild(i).getChild(j).getText().toString());
				Log.e("ClassGetName  : ", (String) currentNode.getChild(i).getChild(j).getClassName());
				if(currentNode.getChild(i).getChild(j).getClass().equals(EditText.class.getName())){

					Log.e(TAG, currentNode.getChild(i).getChild(j).getText().toString());
					Log.e(TAG, "Found!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				}
			}
			Log.e(TAG, "********");
		}
//		Log.e(TAG, "TextToString : " + currentNode.getClass());


//			Log.e(TAG, String.format("onAccessibilityEvent: type = [ %s ], class = [ %s ], package = [ %s ], time = [ %s ], text = [ %s ]",
//					event.getEventType(), event.getClassName(), event.getPackageName(),
//					event.getEventTime(), event.getText()));
//			Log.e(TAG, "###########");
//			Log.e(TAG, event.toString());

	}

	@Override
	public void onInterrupt() {

	}

	@Override
	protected void onServiceConnected() {
		AccessibilityServiceInfo info = new AccessibilityServiceInfo();
//		info.eventTypes = AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED;

		Intent browserIntent = new Intent("android.intent.action.VIEW", Uri.parse("http://"));
		ResolveInfo resolveInfo = getPackageManager().resolveActivity(browserIntent, PackageManager.MATCH_DEFAULT_ONLY);
		String packageName = resolveInfo.activityInfo.packageName;

		String hi = resolveInfo.loadLabel(getPackageManager()).toString();

		startActivity(getPackageManager().getLaunchIntentForPackage("com.android.browser"));
		// This is the default browser's packageName

		info.packageNames = new String[]{"com.android.chrome", "org.mozilla.firefox", "org.mozilla.firefox.App", packageName};

		info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
		info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
		info.notificationTimeout = 100;
		setServiceInfo(info);
	}
}
